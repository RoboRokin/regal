var body = document.body;
var map = document.getElementById("map");
var game = document.getElementById("game");
var mapBg = document.getElementById("map-bg");
var button = document.getElementById("button");

var speed = 1000;
var clicks = 0, hits = 0, missed = 0, late = 0, time = 0;
var width = map.offsetWidth;
var height = map.offsetHeight;
var interval;

body.onresize = function(){resize();}
mapBg.onclick = miss;
button.onclick = hit;
update();

function update () {
	document.getElementById("stat-hits").innerHTML = hits;
	document.getElementById("stat-speed").innerHTML = Math.floor(speed);
	document.getElementById("stat-late").innerHTML = late;
	document.getElementById("stat-clicks").innerHTML = clicks;
	document.getElementById("stat-missed").innerHTML = missed;
	document.getElementById("stat-size").innerHTML = width + "x" + height;
}

function resize () {
	width = map.offsetWidth;
	height = map.offsetHeight;
	update();
}

function move () {
	var x = Math.floor(Math.random() * (width - 60));
	var y = 60 + Math.floor(Math.random() * (height - 120));
	button.style.left = x + "px";
	button.style.top = y + "px";
	time = 0;
	update();
}

function hit () {
	clearInterval(interval);
	clicks++;
	hits++;
	speed /= 1.1;
	move();
	interval = setInterval (tick, 1);
}

function miss () {
	clicks++;
	missed++;
	speed *= 1.1;
	update();
}

function tick () {
	time++;
	document.getElementById("stat-time").innerHTML = time;

	var perc = Math.floor(time / speed * 100);
	perc = Math.floor(perc * 0.72);
	button.style.background = "radial-gradient(red " + perc + "%, white " + perc + "%)";

	if (time >= speed) {
		late++;
		move();
	}
}
